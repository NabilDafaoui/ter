package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.repositories.SujetRepository;

import java.util.Optional;

@Service
public class SujetService {

    static SujetRepository sujetRepository;

    public Iterable<Sujet> getSujets() {
        return sujetRepository.findAll();
    }

    public static Sujet saveSujet(Sujet sujet) {
        Sujet savedSujet = sujetRepository.save(sujet);
        return savedSujet;
    }

    public static Optional<Sujet> findById(long id) {
        return sujetRepository.findById(id);
    }


}