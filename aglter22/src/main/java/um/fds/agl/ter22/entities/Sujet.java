package um.fds.agl.ter22.entities;

import javax.persistence.*;


@Entity
public class Sujet {
    private String titre;
    private @ManyToOne Teacher LastNameProf;

    @ManyToOne
    @JoinColumn(name = "encadrant_id")
    private Teacher Encadrant;
    private @Id @GeneratedValue Long id;

    public Teacher getEncadrant() {
        return Encadrant;
    }

    public void setEncadrant(Teacher Encadrant) {
        this.Encadrant = Encadrant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sujet(String titre, Teacher LastNameProf, Long id) {
        this.titre = titre;
        this.LastNameProf = LastNameProf;
        this.id = id;
    }

    public Sujet(String titre, Teacher LastNameProf){
        this.titre=titre;
        this.LastNameProf=LastNameProf;

    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Teacher getLastNameProf() {
        return LastNameProf;
    }

    public void setLastNameProf(Teacher LastNameProf) {
        this.LastNameProf = LastNameProf;
    }

    public Sujet() {
    }


}